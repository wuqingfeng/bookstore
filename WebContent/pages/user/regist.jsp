<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会员注册页面</title>
<%@include file="/WEB-INF/include/base.jsp" %>
<script type="text/javascript" src="static/script/jquery-1.8.0.js"></script>
<style type="text/css">
	.login_form{
		height:420px;
		margin-top: 25px;
	}
	
</style>

<script type="text/javascript">
	$(function(){
		
		//为username 的文本框绑定一change时间
		$("[name=username]").change(function(){
			//设置一个url 
			var url="${pageContext.request.contextPath}/client/UserServlet";
			//设置请求参数
			var params={"method":"checkUsername","username":this.value};
			
			//发送请求
			$.post(url,params,function(data){
				
				if(data == 0){
					//用户名不可用，设置错误消息
					$(".errorMsg").html("用户名已经存在");
					//然按钮变为不可操作的状态
    				$("#sub_btn").attr("disabled",true);
				}else if(data==1){
					//用户可用
					$(".errorMsg").html("");
					//然后 按钮变为可以操作
					$("#sub_btn").attr("disabled",false);
				}
			},"json");
		});
		
		
		$("#code_img").click(function(){
			this.src="code.jpg?t="+Math.random();
		});

		
	});


	$(document).ready(function(e) {
        //为提交按钮绑定单击响应函数
		$("#sub_btn").click(function(e) {
            //获取输入的用户名 密码 确认密码 电子邮件 验证码
			var name=$("[name=username]").val();
			var password=$("[name=password]").val();
			var repwd=$("[name=repwd]").val();
			var email=$("[name=email]").val();
			var code=$("[name=code1]").val();
			

			
			//检查用户名 密码等信息是否符合规则
			//验证 用户名
			var nameReg=/^[a-z0-9_-]{3,16}$/;
			if(!nameReg.test(name)){
				//用户名不正确
				
				$("span.errorMsg").text("用户名格式不正确");
				return false;	
			}
			
			var pwdReg=/^[a-z0-9_-]{6,18}$/;
			if(!pwdReg.test(password)){
				//密码错误
				$("span.errorMsg").text("密码格式错误！");
				return false;	
			}
			
		//alert($.trim(password) + "=?" + $.trim(repwd));
			if($.trim(password) != $.trim(repwd)){
				
				$("span.errorMsg").text("密码输入不一致");
				return false;
			}
			
			//检查邮箱格式
			var emailReg=/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
			if(!emailReg.test(email)){
				//
				$("span.errorMsg").text("邮箱格式有误！");
				return false;	
			}
			
			
			//取消默认行为
			//return false;
        });
    });
</script>

</head>
<body>
		<div id="login_header">
			<img class="logo_img" alt="" src="static/img/logo.png" >
		</div>
		
			<div class="login_banner">
			
				<div id="l_content">
					<span class="login_word">欢迎注册</span>
				</div>
				
				<div id="content">
					<div class="login_form">
						<div class="login_box">
							<div class="tit">
								<h1>注册会员</h1>
								<span class="errorMsg">${msg }</span>
							</div>
							<div class="form">
								<form action="client/UserServlet?method=regist" method="post">
									
									<label>用户名称：</label>
									<input class="itxt" type="text" placeholder="请输入用户名" autocomplete="off" tabindex="1" name="username"
										value="${param.username }" />
									<br />
									<br />
									<label>用户密码：</label>
									<input class="itxt" type="password" placeholder="请输入密码" autocomplete="off" tabindex="1" name="password" 
										/>
									<br />
									<br />
									<label>确认密码：</label>
									<input class="itxt" type="password" placeholder="确认密码" autocomplete="off" tabindex="1" name="repwd" 
										/>
									<br />
									<br />
									<label>电子邮件：</label>
									<input class="itxt" type="text" placeholder="请输入邮箱地址" autocomplete="off" tabindex="1" name="email"
										value="${param.email }" />
									<br />
									<br />
									<label>验证码：</label>
									<input class="itxt" type="text" style="width: 150px;" name="code1"/>
									<img id="code_img" alt="" src="code.jpg" style="float: right; margin-right: 40px ; width:90px; heigth:45px" name="code">									
									<br />
									<br />
									<input type="submit" value="注册" id="sub_btn" />
									
								</form>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		<div id="bottom">
			<span>
				东方标准.Copyright &copy;2016
			</span>
		</div>
</body>
</html>