<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会员登录页面</title>
<%@include file="/WEB-INF/include/base.jsp" %>
</head>
<body>
		<div id="login_header">
			<img class="logo_img" alt="" src="static/img/logo.png" >
		</div>
		
			<div class="login_banner">
			
				<div id="l_content">
					<span class="login_word">欢迎登录</span>
				</div>
				
				<div id="content">
					<div class="login_form">
						<div class="login_box">
							<div class="tit">
								<h1>会员</h1>
								<a href="pages/user/regist.jsp">立即注册</a>
							</div>
							<div class="msg_cont">
								<b></b>
								<span class="errorMsg">
								<%-- 
								<%=request.getAttribute("msg") ==null? "请输入用户名和密码": request.getAttribute("msg") %>
								${param.msg }
								
								--%>
								${empty msg?"请输入用户名和密码！":msg}
								</span>
							</div>
							<div class="form">
								<form action="client/UserServlet?method=login " method="get">
									<!-- 若请求为get时  浏览器上面的写法无效 为method赋值的方法无效 ，因此要用hidden的方式赋值   -->
									<!-- post 则可以 -->
									
								<!-- 
								当使用get 请求时，get请求会覆盖掉action 中的请求参数， 可以在表单中隐藏域，
								name 值就是method ,值就是要调用的方法
								<input type="hidden" name="method" value="login" />
																	
								 -->
									
									
									<input type="hidden" name="method" value="login">
									<label>用户名称：</label>
									<input class="itxt" type="text" placeholder="请输入用户名" autocomplete="off" tabindex="1" name="username" 
									value="${param.username}" />
									<br />
									<br />
									<label>用户密码：</label>
									<input class="itxt" type="password" placeholder="请输入密码" autocomplete="off" tabindex="1" name="password"
									value="${param.password}"  />
									<br />
									<br />
									<input type="submit" value="登录" id="sub_btn" />
								</form>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		<div id="bottom">
			<span>
				东方标准.Copyright &copy;2016
			</span>
		</div>
</body>
</html>