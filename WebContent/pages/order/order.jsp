<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>我的订单</title>
<%@include file="/WEB-INF/include/base.jsp"%>
<style type="text/css">
h1 {
	text-align: center;
	margin-top: 200px;
}
</style>
</head>
<body>

	<div id="header">
		<img class="logo_img" alt="" src="../../static/img/logo.png"> <span
			class="wel_word">我的订单</span>
		<%@ include file="/WEB-INF/include/manager-info.jsp"%>
	</div>

	<div id="main">
		<c:choose>
			<c:when test="${empty list }">
				<h1>当前用户没有订单</h1>
			</c:when>

			<c:otherwise>


				<table>
					<tr>
						<td>订单号</td>
						<td>标题</td>
						<td>日期</td>
						<td>数量</td>
						<td>金额</td>
						<td>状态</td>
						<td>详情</td>
					</tr>

					<c:forEach items="${list }" var="order">
						<tr>
							<td>${order.id }</td>
							<td>${order.id }</td>
							<td>
								<%--
					可以使用fmt标签对日期进行格式化
					value 指向要格式化的那个时间
					type 要显示的事件类型
						date 日期
						time 时间
						both 都显示
					 --%> <fmt:formatDate value="${order.orderTime }" type="both" />
							</td>

							<td>${order.totalCount }</td>
							<td>${order.totalAmount }</td>
							<td><c:choose>
									<c:when test="${order.state ==0 }">
										<span>未发货</span>
									</c:when>
									<c:when test="${order.state ==1 }">
										<a
											href="client/OrderClientServlet?method=takeBook&orderId=${order.id }">确认收货</a>
									</c:when>
									<c:when test="${order.state ==2 }">
										<span>交易完成</span>
									</c:when>
								</c:choose></td>

							<td><a href="client/OrderClientServlet?method=orderInfo&orderId=${order.id }">查看详情</a></td>
						</tr>

					</c:forEach>
				</table>



			</c:otherwise>

		</c:choose>



	</div>

	<div id="bottom">
		<span> 东方标准.Copyright &copy;2016 </span>
	</div>
</body>
</html>