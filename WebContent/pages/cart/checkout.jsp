<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>结算页面</title>
<%@include file="/WEB-INF/include/base.jsp" %>
<style type="text/css">
	h1 {
		text-align: center;
		margin-top: 200px;
	}
</style>
</head>
<body>
	
	<div id="header">
			<img class="logo_img" alt="" src="../../static/img/logo.png" >
			<span class="wel_word">结算</span>
			 <%@ include file="/WEB-INF/include/user-info.jsp" %> 
	</div>
	
	<div id="main">
		
		<h1>你的订单已结算，订单号为  
			<span style="color:red">${orderId }</span>
		</h1>
		
	
	</div>
	
	<div id="bottom">
		<span>
			东方标准.Copyright &copy;2016
		</span>
	</div>
</body>
</html>