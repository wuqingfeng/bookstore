<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>购物车</title>
<%@include file="/WEB-INF/include/base.jsp" %>
<script type="text/javascript">
	$(function(){
		$("#clearCart").click(function(){
			if(!confirm("清空購物車？")){
				//取消默認行為
				return false;
				
			}
			
		})
		
		
		//為所有得刪除按鈕綁定一個單機餉銀函數
		$(".delA").click(function(){
			//獲取要刪除的圖書的名字
			var title =$(this).parents("tr").find("td :eq(0)").html();
			if(!confirm("確認要刪除  《"+title +"》 嗎?")){
				return false;
			}
			
			
		})
		
		//为class为 count_input 确定一个change事件
		$(".count_input").change(function(){
			//获取修改后的数量
			var count=this.value;
			//检查count是否为一个整数
			//alert(count);
			var reg=/^\+?[1-9][0-9]*$/;
			if(!reg.test(count)){
				//值非法  变成原来的值
				this.value=this.defaultValue;
				//弹出一个提示框
				alert("请输入一个正确的数量 ");
				return;
			}
			
			//获取id
			var bookId=this.name;
			//创建一个url
			var url="${pageContext.request.contextPath}/client/CartServlet";
			//设置一个请求参数
			var params={"method":"updateCount",
						"bookId":bookId,
						"count":count
			};
			
			//获取金额所在的Id
			var $td= $(this).parents("tr").find("td:eq(3)");
			//发送ajax请求
			$.post(url,params,function(data){
				//修改商品的总数据
    			$(".b_count").html(data.totalCount );
    			//修改商品的总金额
    			$(".b_price").html(data.totalAmount);
    			//修改商品的小计金额
    			$td.html(data.amount);
    			
				
			},"json")
			//向servlet发送请求
			/* window.location="${pageContext.request.contextPath}"
			+"/client/CartServlet?method=updateCount&bookId="
			+ bookId + "&count=" + count; */
			
			
		})
		
		
	})

</script>


</head>
<body>
	
	<div id="header">
			<img class="logo_img" alt="" src="../../static/img/logo.png" >
			<span class="wel_word">购物车</span>
			 <%@ include file="/WEB-INF/include/user-info.jsp" %> 
	</div>
	
	<div id="main">
	
	<c:choose>
	
		<c:when test="${empty cart.cartItems }">
			<br /><br /><br /><br /><br /><br /><br />
			<h1 align="center">购物车中没有商品</h1>
		</c:when>
		<c:otherwise>
		
		<table>
			<tr>
				<td>商品名称</td>
				<td>数量</td>
				<td>单价</td>
				<td>金额</td>
				<td>操作</td>
			</tr>	
		
		<c:forEach items="${cart.cartItems }" var="cartItem">
		
		<tr>
				<td>${cartItem.book.title }</td>
				<td>
					<input name="${cartItem.book.id }" class="count_input" type="text"
					value="${cartItem.count }" style="width:50px;text-align: center"
					/>
				
				
				</td>
				
				<td>${cartItem.book.price }</td>
				<td>${cartItem.amount }</td>
				<td><a class="delA" href="client/CartServlet?method=delCartItem&bookId=${cartItem.book.id }">删除</a></td>
			</tr>	
		
		</c:forEach>
		
		</table>
		
		<div class="cart_info">
			<span class="cart_span">购物车中共有<span class="b_count">${cart.totalCount }</span>件商品</span>
			<span class="cart_span">总金额<span class="b_price">${cart.totalAmount }</span>元</span>
			<span class="cart_span"><a id="clearCart" href="client/CartServlet?method=clear">清空购物车</a></span>
			<span class="cart_span"><a href="client/OrderClientServlet?method=checkOut">去结账</a></span>
		</div>
		
		
		
		</c:otherwise>
	</c:choose>
		
	
	</div>
	
	<div id="bottom">
		<span>
		东方标准.Copyright &copy;2016
		</span>
	</div>
</body>
</html>