package cn.ntrj.bookstore.bean;

/**
 * 订单列表
 * 
 * @author wp
 *
 */
public class OrderItem {
	private Integer id;// 订单项编号
	private int count;// 当前图书数量
	private double amount;// 当前图书的金额
	private String title;// 书名
	private String author;//
	private double price;
	private String imgPath;// 封面
	private String order_id;

	public OrderItem() {
		super();
	
	}

	public OrderItem(Integer id, int count, double amount, String title, String author, double price, String imgPath,
			String order_id) {
		super();
		this.id = id;
		this.count = count;
		this.amount = amount;
		this.title = title;
		this.author = author;
		this.price = price;
		this.imgPath = imgPath;
		this.order_id = order_id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	@Override
	public String toString() {
		return "OrderItem [id=" + id + ", count=" + count + ", amount=" + amount + ", title=" + title + ", author="
				+ author + ", price=" + price + ", imgPathl=" + imgPath + ", order_id=" + order_id + "]";
	}

}
