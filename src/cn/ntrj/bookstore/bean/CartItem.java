package cn.ntrj.bookstore.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 封装购物项信息的类
 * @author wp
 *
 */
public class CartItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Book book;//图书信息
	private int count;//单品种图书的数量
	/**
	 * 金额 =单价*数量
	 */
//	private double amount;//单品种图书的金额
	public CartItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	/**
	 * 商品总金额 = 单价 * 数量
	 * 即 amount = getPrice() * getCount()
	 * @return
	 */
	public double getAmount() {
		//计算商品总金额
	//	double amount= book.getPrice() *getCount();
		BigDecimal price=new BigDecimal(book.getPrice()+"");
		BigDecimal amount=new BigDecimal(getCount());
		
		//计算商品的总金额
		return price.multiply(amount).doubleValue();
	}

	public CartItem(Book book, int count) {
		super();
		this.book = book;
		this.count = count;
	}

	@Override
	public String toString() {
		return "CartItem [book=" + book + ", count=" + count + "]";
	}
	
	

	

	

}
