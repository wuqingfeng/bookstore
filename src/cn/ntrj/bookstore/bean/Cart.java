package cn.ntrj.bookstore.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 封装了 购物信息
 * @author wp
 *
 */
public class Cart implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 在map中保存购物项信息
	 * key 是String 类型的bookId
	 * value :CartItem
	 * 
	 * 用LinkedHashMap()  是希望购物车中的信息是有序的
	 */
	private Map<String, CartItem> map=new LinkedHashMap<>();
	//商品的总数量
	//private int totalCount;
	
	/**
	 * 商品总金额
	 */
//	private double totalAmount;
	/**
	 * 获取所有得购物项
	 * @return
	 */
	public List<CartItem> getCartItems(){
		//获取所有map中的value
		Collection<CartItem> values=map.values();
		//将collection对象转换长ArrayList
		List<CartItem> cartItems=new ArrayList<>(values);
		//返回cartItems
		return cartItems;
		
	}
	public Map<String, CartItem> getMap() {
		return map;
	}
	public void setMap(Map<String, CartItem> map) {
		this.map = map;
	}
	/**
	 *  商品总数量
	 * @return
	 */
	public int getTotalCount() {
		//计算商品的总数
		int totalCount = 0;
		List<CartItem> cartItems =getCartItems();
		//遍历
		for (CartItem cartItem : cartItems) {
			//计算数量
			totalCount += cartItem.getCount();
		}
		return totalCount;
	}


	
	public double getTotalAmount() {
		BigDecimal totalAmount=new BigDecimal("0");
		//double  totalAmount=0;
		//获取所有的购物项
		List<CartItem> cartItems =getCartItems();
		for (CartItem cartItem : cartItems) {
			//计算总金额
		//	totalAmount += cartItem.getAmount();
			//将CartItem.getAmount 包装为一个BigDecimal 对象
			BigDecimal amount=new BigDecimal(cartItem.getAmount()+"");
			totalAmount=totalAmount.add(amount);
		}
		
		
		return totalAmount.doubleValue();
	}
	/**
	 * 删除一个购物项
	 * @param bookId
	 */
	public void delCartItem(String bookId) {
		//从map 移除指定的购物项
		map.remove(bookId);
		
	}
	
	/**
	 * 清空购物车
	 */
	public void clear() {
		//清空map
		map.clear();
	}
	
	/**
	 * 向购物车里面添加一本图书
	 * @param book
	 */
	public void addBookToCart(Book book) {
		//从购物车获取指定的购物项
		CartItem cartItem=map.get(book.getId() + "");
		//判断cartItem是否为null
		if(cartItem == null) {
			
			cartItem =new CartItem();
			
			cartItem.setBook(book);
			cartItem.setCount(1);
			map.put(book.getId().toString(), cartItem);
					
		}else{
			//cartItem 不为null 购物车中已有该购物项，需要在购物项的数量上加1
			int count=cartItem.getCount();
			cartItem.setCount(count+1);
			
		}
	}
	
	/**
	 * 修改指定购物项的数量
	 * @param bookId
	 * @param countStr
	 */
	public void  updateCount(String bookId,String countStr) {
		//指定一个默认值
		int count=1;
		
		try {
			//将countStr转型为int  失败则不转换 默认为1
			count=Integer.parseInt(countStr);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		//获取要修改的购物项
		CartItem cartItem=map.get(bookId);
		cartItem.setCount(count);
	}
	
	

}
