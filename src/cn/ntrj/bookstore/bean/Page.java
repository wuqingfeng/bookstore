package cn.ntrj.bookstore.bean;
/**
 * 封装分页信息
 * @author wp
 *
 * @param <T>
 */

import java.util.List;

public class Page<T> {
	/**
	 * 当前页面
	 */
	private int pageNumber;
	/**
	 * 每页显示的条数
	 */
	private int pageSize;
//	private int totalPage;//总页数
	private int totalRecord;//总记录数
//	private int index;//索引计算所得的位置
	
	private List<T> data;//分页数据
	/**
	 * 保存请求地址的消息
	 */
	private String path;
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getPageNumber() {
		if(pageNumber < 1) {
		//	System.out.println("getPageNumber  pageNumber  < 1");
			return 1;
		}
		if(pageNumber > getTotalPage()) {
		//	System.out.println("getPageNumber  pageNumber > getTotalPage()");
			return getTotalPage();
		}
		
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 计算总页数
	 * 总记录数 每页的条数  总页数
	 * 10		2		5
	 * 
	 * totalPage=totalRecord/ pageSize
	 * @return
	 */
	
	
	public int getTotalPage() {
		if (getTotalRecord() % getPageSize() == 0){
			return  getTotalRecord() / getPageSize();
		} else {
			return getTotalRecord() / getPageSize() + 1;
		}
	}
	
	public int getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}

	/**
	 * 计算index值
	 * pageNumber index pageSize
	 * 	1			0		3
	 * 	2			3		3
	 * 	3			6		3
	 * 
	 * index=(pageNumber -1) *pageSize;
	 * @return
	 */
	public int getIndex() {
		
	//	System.out.println(pageNumber);
//		return (pageNumber -1) *pageSize;
		return (getPageNumber() -1 )*getPageSize();
	}

	

	/**
	 * 获取数据（数据库中）
	 * @return
	 */
	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}
	
	

}
