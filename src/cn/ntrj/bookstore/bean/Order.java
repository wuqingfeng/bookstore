package cn.ntrj.bookstore.bean;

import java.util.Date;

/**
 * 封装订单信息
 * @author wp
 *
 */
public class Order {
	//订单号  时间戳+""+用户id
	private String id;
	private Date orderTime;//订单生成的事件
	private int totalCount;//商品总数量
	private double totalAmount;//商品总金额
	/**
	 * 订单状态：  0未发货  1 已发货   2交易完成
	 */
	private int state;
	private int user_id;
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Order(String id, Date orderTime, int totalCount, double totalAmount, int state, int user_id) {
		super();
		this.id = id;
		this.orderTime = orderTime;
		this.totalCount = totalCount;
		this.totalAmount = totalAmount;
		this.state = state;
		this.user_id = user_id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	@Override
	public String toString() {
		return "Order [id=" + id + ", orderTime=" + orderTime + ", totalCount=" + totalCount + ", totalAmount="
				+ totalAmount + ", state=" + state + ", user_id=" + user_id + "]";
	}
	
	

}
