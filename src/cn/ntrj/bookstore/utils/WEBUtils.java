package cn.ntrj.bookstore.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

import cn.ntrj.bookstore.bean.Cart;


/**
 * WEB相关操作的 工具类
 * @author wp
 *
 */

public class WEBUtils {
	
	/**
	 * 将请求参数封装为一个对象  并且返回
	 * @param request
	 * @param t
	 * @return 
	 * @return
	 */
	
	public static <T> T param2Bean(HttpServletRequest request,T t){
		
		Map<String, String[]> map=request.getParameterMap();
		try {
			BeanUtils.populate(t, map);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return t;
	}
	
	
	/**
	 * 获取请求的地址名
	 * @param request
	 * @param response
	 * @return
	 */
	public static String getPath(HttpServletRequest request,HttpServletResponse response) {
		//动态获取请求的求地址
		String uri=request.getRequestURI();
		//System.out.println(uri);///BookStore_DF5/manager/BookManagerServlet
		//获取查询字符串
		String queryString=request.getQueryString();
	//	System.out.println(queryString);//method=findBook
		String path=uri +"?" + queryString;
	//	System.out.println(path);
		
		//判断path中是否包含 &pagenumber字符串
		if(path.contains("&pageNumber")) {
			path=path.substring(0, path.indexOf("&pageNumber"));
		}
		return path;
	}


	public static Cart getCart(HttpServletRequest request) {
		//获取session
				HttpSession session=request.getSession();
				
				//从session中获取购物车对象 
				Cart cart=(Cart) session.getAttribute("cart");
				//判断cart 是否为null
				if(cart == null) {
					//创建一个新的cart对象
					cart =new Cart();
					//将cart添加到session域中
					session.setAttribute("cart", cart);
				}
				
		return cart;
	}

}
