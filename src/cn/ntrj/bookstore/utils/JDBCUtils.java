package cn.ntrj.bookstore.utils;
/**
 * 获取和释放数据库连接的工具类
 * @author wp
 *
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class JDBCUtils {
	private static DataSource ds = new ComboPooledDataSource("helloc3p0");
	// 创建一个Connection的属性 单例的
	// private static Connection conn;
	// HashMap 线程不安全 ConcurrentHashMap线程安全 因此此方法可行
	// private static Map<Thread, Connection> map = new ConcurrentHashMap<>();

	// ThreadLocal 对象在 内部维护者一个map， 这个map 中的 key 就是当前线程，而值我们可以定义为任意类型
	// ThreadLocal 中用来在当前线程中共享对象，一个ThreadLocal 在当前线程中只能存一个值
	private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();

	/**
	 * 获取数据库连接
	 */
	public static Connection getConnection() {
		// 从map中 获取当前线程的connection对象
		Connection conn = threadLocal.get();
		// 判断map是否为null
		if (conn == null) {
			try {
				conn = ds.getConnection();
				// 将conn 放入map
				threadLocal.set(conn);
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return conn;

		/*
		 * // 从map中 获取当前线程的connection对象 Connection conn =
		 * map.get(Thread.currentThread()); // 判断map是否为null if (conn == null) { try {
		 * conn = ds.getConnection(); // 将conn 放入map map.put(Thread.currentThread(),
		 * conn); } catch (SQLException e) { 
		 * e.printStackTrace(); } } return conn;
		 */

		/*
		 * // 判断conn 是否为null if (conn == null) { try { conn = ds.getConnection(); }
		 * catch (Exception e) { e.printStackTrace(); } }
		 * 
		 * return conn;
		 */
	}

	public static void releaseConnection(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void releaseConnection() {
		// 获取当前对象的conn对象
		Connection conn = threadLocal.get();
		if (conn != null) {
			try {
				conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		threadLocal.remove();

		/*
		 * // 获取当前对象的conn对象 Connection conn = map.get(Thread.currentThread()); if (conn
		 * != null) { try { conn.close(); map.remove(Thread.currentThread()); } catch
		 * (SQLException e) { e.printStackTrace(); }
		 * }
		 */

		/*
		 * 
		 * if(conn !=null) { try { conn.close(); } catch (SQLException e) { 
		 * Auto-generated catch block e.printStackTrace(); } }
		 * 
		 * conn = null;
		 */
	}

}
