package cn.ntrj.bookstore.dao;

import java.util.List;

import cn.ntrj.bookstore.bean.Order;
import cn.ntrj.bookstore.bean.OrderItem;

/**
 * 用来定义订单的基本操作方法
 * @author wp
 *
 */
public interface OrderDao {
	/**
	 * 保存订单信息
	 * @param order
	 * @return
	 */
	int saveOrder(Order order);
	/**
	 * 修改订单状态
	 * @param OrderId
	 * @param state
	 * @return
	 */
	int updateOrderState(String orderId,int state);
	
	/**
	 * 查询所有订单 管理员
	 * @return
	 */
	List<Order> getOrderList();
	

	/**
	 * 根据用户的id查找订单 普通用户调用
	 * @param userId
	 * @return
	 */
	List<Order> getOrderListByUserId(String userId);
	

	
	

}
