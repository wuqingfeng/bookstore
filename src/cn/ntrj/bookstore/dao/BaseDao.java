package cn.ntrj.bookstore.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import cn.ntrj.bookstore.utils.JDBCUtils;

/**
 * 定义一些数据库的基本操作
 * 是专门用于被其他Dao 继承的
 * @author wp
 *
 * @param <T>
 */
public class BaseDao<T> {
	
	private QueryRunner qr=new QueryRunner();
	private Class<T> type;
	
	/**
	 * 
	 */
	public BaseDao() {
	//	System.out.println(this.toString());
//		System.out.println("BaseDao()");
		// 创建一个无参的构造器，这个构造器是由子类调用
		// UserDao extends BaseDao<User>
		// 获取当前子类的类型
		Class cla=this.getClass();
		//获取父类的类型
		ParameterizedType pt=(ParameterizedType) cla.getGenericSuperclass();
		//获取所有的泛型
		Type[] types=pt.getActualTypeArguments();
		this.type=(Class<T>)types[0];
	//	System.out.println(this.type);
	
	}
	
	/**
	 * 查询一个对象
	 * @param sql
	 * @param params
	 * @return
	 */
	public T getBean(String sql,Object...params) {
		T t=null;
		//获取数据库连接
		Connection conn=JDBCUtils.getConnection();
		try {
			t=qr.query(conn, sql, new BeanHandler<>(type),params);
		} catch (Exception e) {
			//将异常转换为运行时异常往上抛
			throw new RuntimeException(e);
			//e.printStackTrace();
		}/*finally {
			JDBCUtils.releaseConnection(conn);
		}*/
	
		return t;
		
	}
	
	/**
	 * 查询一组对象
	 * @param sql
	 * @param params
	 * @return
	 */
	public List<T> getBeanList(String sql,Object...params) {
		List<T> list=null;
		//获取数据库连接
		Connection conn=JDBCUtils.getConnection();
		try {
			list=qr.query(conn, sql, new BeanListHandler<>(type),params);
		} catch (Exception e) {
			//将异常转换为运行时异常往上抛
			throw new RuntimeException(e);
			//e.printStackTrace();
		}/*finally {
			JDBCUtils.releaseConnection(conn);
		}*/
	
		return list;	
	}
	
	/**
	 * 更新数据库操作的方法
	 * @param sql
	 * @param params
	 * @return
	 */
	public int update(String sql,Object...params) {
		
		int count=0;
		Connection conn=JDBCUtils.getConnection();
		try {
			count=qr.update(conn, sql, params);
		} catch (Exception e) {
			//将异常转换为运行时异常往上抛
			throw new RuntimeException(e);
		//	e.printStackTrace();
		}/*finally {
			JDBCUtils.releaseConnection(conn);
		}*/
		
		return count;
		
	}
	
	/**
	 * 从数据库中查询一个单一的值
	 * @param sql
	 * @param params
	 * @return
	 */
	public Object  getSingleValue(String sql,Object...params) {
		
		Object obj=null;
		Connection conn=JDBCUtils.getConnection();
		try {
			//ScalarHandler : ,...
			obj=qr.query(conn, sql,  new ScalarHandler(),params);
		} catch (Exception e) {
			//将异常转换为运行时异常往上抛
			throw new RuntimeException(e);
			//e.printStackTrace();
		}/*finally {
			JDBCUtils.releaseConnection(conn);
		}*/
		
		return obj;
		
	}
	
	/**
	 * 批量操作數據庫的方法
	 * @param sql
	 * @param params
	 * 
	 * Object[][] params 是一个二维数组
	 * 					二维数组的第一维：sql 语句要执行的次数
	 * 					二维数组的第二维：每次执行sql语句使用的参数
	 */
	public int[] batchUpdate(String sql , Object[][] params) {
		
		Connection conn=JDBCUtils.getConnection();
		int[] counts=null;
		try {
			//ScalarHandler : ,...
			counts=qr.batch(conn,sql, params);
		} catch (Exception e) {
			//将异常转换为运行时异常往上抛
			throw new RuntimeException(e);
			//e.printStackTrace();
		}/*finally {
			JDBCUtils.releaseConnection(conn);
		}*/
		
		return counts;
		
	}
	
	
	
}
