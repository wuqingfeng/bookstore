package cn.ntrj.bookstore.dao.impl;

import java.util.List;

import cn.ntrj.bookstore.bean.OrderItem;
import cn.ntrj.bookstore.dao.BaseDao;
import cn.ntrj.bookstore.dao.OrderItemDao;

/**
 * OrderItemDao ��ʵ����
 * @author wp
 *
 */
public class OrderItemDaoImpl extends BaseDao<OrderItem> implements OrderItemDao {

	@Override
	public int saveOrderItem(OrderItem orderItem) {
		String sql="insert into bs_order_item(count,amount,title,author,price,img_path,order_id)"
				+ "values(?,?,?,?,?,?,?)";
		return this.update(sql, orderItem.getCount(), orderItem.getAmount(), orderItem.getTitle(),
				orderItem.getAuthor(), orderItem.getPrice(), orderItem.getImgPath(), orderItem.getOrder_id());
	}

	@Override
	public List<OrderItem> getOrderItemByOrderId(String orderId) {
		String sql="select id,count,amount,title,author,price,img_path imgPath,order_id "
				+ " from bs_order_item "
				+ "where order_id = ?";
		return this.getBeanList(sql, orderId);
	}

	@Override
	public int[] batchSave(Object[][] params) {
		String sql="insert into bs_order_item(count,amount,title,author,price,img_path,order_id) " +
		         " values(?,?,?,?,?,?,?)";;
		return this.batchUpdate(sql, params);
		
	}
	
	

}
