package cn.ntrj.bookstore.dao.impl;

import cn.ntrj.bookstore.bean.User;
import cn.ntrj.bookstore.dao.BaseDao;
import cn.ntrj.bookstore.dao.UserDao;

public class UserDaoImpl extends BaseDao<User> implements UserDao {

	@Override
	public User getUserByUsernameAndPassword(User user) {
		String sql="select id,username,password,email "
				+ "from bs_user "
				+ "where username=? and password=? ";
		
		return this.getBean(sql, user.getUsername(),user.getPassword());
	}

	@Override
	public int SaveUser(User user) {
		String sql="insert into bs_user(username,password,email) value(?,?,?)";
		try {
			return this.update(sql, user.getUsername(),user.getPassword(),user.getEmail());
		} catch (Exception e) {
			//e.printStackTrace();
			return 0;
		}
		
	}

	@Override
	public User getUserByUsername(String username) {
		String sql="select id,username,password,email from bs_user where username=?";
		return this.getBean(sql, username);
	}

}
