package cn.ntrj.bookstore.dao.impl;

import java.util.List;

import cn.ntrj.bookstore.bean.Order;
import cn.ntrj.bookstore.bean.OrderItem;
import cn.ntrj.bookstore.dao.BaseDao;
import cn.ntrj.bookstore.dao.OrderDao;

public class OrderDaoImpl extends BaseDao<Order> implements OrderDao {

	@Override
	public int saveOrder(Order order) {
		String sql="insert into bs_order(id,order_time,total_count,total_amount,state,user_id)"
				+ "values(?,?,?,?,?,?)";
		return this.update(sql, order.getId(),order.getOrderTime(),order.getTotalCount(),order.getTotalAmount(),order.getState(),order.getUser_id());
	}

	@Override
	public int updateOrderState(String orderId, int state) {
		String sql="update bs_order set state = ? where id = ? ";
		return this.update(sql, state,orderId);
	}

	@Override
	public List<Order> getOrderList() {
		String sql="select id,order_time orderTime,total_count totalCount,total_amount totalAmount,state,user_id from bs_order "+
	              "  order by order_time desc";
		return this.getBeanList(sql);
	}

	@Override
	public List<Order> getOrderListByUserId(String userId) {
		String sql="select id,order_time orderTime,total_count totalCount,total_amount totalAmount,state,user_id from bs_order "+
	              "   where user_id = ? order by order_time desc";
		return this.getBeanList(sql, userId);
	}

	

	

	
}
