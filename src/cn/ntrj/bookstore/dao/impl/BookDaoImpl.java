package cn.ntrj.bookstore.dao.impl;

import java.util.List;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Page;
import cn.ntrj.bookstore.dao.BaseDao;
import cn.ntrj.bookstore.dao.BookDao;
/**
 * BookDao 的实现类
 * @author wp
 *
 */
public class BookDaoImpl extends BaseDao<Book> implements BookDao {

	@Override
	public int saveBook(Book book) {
		String sql="insert into bs_book(title,author,price,sales,stock,img_path) value(?,?,?,?,?,?) ";
		return this.update(sql,book.getTitle(),book.getAuthor(),
				book.getPrice(),book.getSales(),book.getStock(),book.getImg_path());
	}

	@Override
	public int delBook(String bookId) {
		String sql="delete from bs_book where id=?";
		return this.update(sql, bookId);
	}
	
	@Override
	public int updateBook(Book book) {
		
		String sql="update bs_book set title = ? ,author= ? ,price=?,"
				+ " sales=? ,stock=? ,img_path=? where id=?";
		return this.update(sql, book.getTitle(),book.getAuthor()
				,book.getPrice(),book.getSales(),book.getStock()
				,book.getImg_path(),book.getId());
	}

	@Override
	public List<Book> getBookList() {
		String sql="select id,title,author,price,sales,stock,img_path from bs_book ";
		return this.getBeanList(sql);
	}

	@Override
	public Book getBookById(String bookId) {
		String sql="select id,title,author,price,sales,stock,img_path from bs_book where id=?";
		return this.getBean(sql, bookId);
	}

	/**
	 * 参数中的page是由service传递过来的
	 * page中已经有两个属性  pageNumber  ,pageSize;
	 * 在当前方法中，还需要封装两个参数 totalRecord 和date
	 */
	@Override
	public Page<Book> findBook(Page<Book> page) {
		//获取图书的总记录数
		String sql="select count(*) from bs_book";
		long totalRecord=(long) this.getSingleValue(sql);
		//将总记录数设置进page对象中
		page.setTotalRecord((int)totalRecord);

		//查询图书信息
		sql="select id,title,author,price,sales,stock,img_path from bs_book limit ?,?";
	//	System.out.println( " page.getIndex()::"+page.getIndex());
		List<Book> date=this.getBeanList(sql, page.getIndex(),page.getPageSize());
		
		
		//将数据设置进page中
		page.setData(date);
		return page;
	}

	@Override
	public Page<Book> findBookByPrice(Page<Book> page, double minPrice, double maxPrice) {

		//查找总记录数totalRecord
		String sql="select count(*) from bs_book where price >=? and price <= ?";
		long totalRecord=(long)this.getSingleValue(sql, minPrice, maxPrice);
		
		//将总记录数设置进page对象
		page.setTotalRecord((int)totalRecord);
		
		sql="select id,title,author,price,sales,stock,img_path "
				+ "from bs_book "
				+ "where price >=? and price <= ?"
				+ "limit ?,?";
		List<Book> data=this.getBeanList(sql, minPrice,maxPrice,page.getIndex(),page.getPageSize());
		page.setData(data);
		
		return page;
	}

	@Override
	public int[] batchUpdateSalesAndStock(Object[][] params) {
		String sql="update bs_book set  sales=? ,stock=?  where id=?";
		return this.batchUpdate(sql, params);
		
	}


	

}
