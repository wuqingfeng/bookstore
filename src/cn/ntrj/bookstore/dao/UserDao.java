package cn.ntrj.bookstore.dao;

import cn.ntrj.bookstore.bean.User;

/**
 * 定义User 表的基本操作的Dao
 * @author wp
 *
 */
public interface UserDao {
	
	/**
	 * 根据用户名和密码查询用户
	 * @param user
	 * @return
	 */
	User getUserByUsernameAndPassword(User user);
	
	
	/**
	 * 向数据库插入一个用户对象
	 * @param user
	 * @return
	 */
	int SaveUser(User user);
	
	/**
	 * 根据用户名查找用户对象
	 * @param username
	 * @return
	 */
	User getUserByUsername(String username);
}
