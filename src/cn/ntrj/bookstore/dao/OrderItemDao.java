package cn.ntrj.bookstore.dao;

import java.util.List;

import cn.ntrj.bookstore.bean.OrderItem;
/**
 * 定义订单项的数据库操作的接口
 * @author wp
 *
 */
public interface OrderItemDao {
	
	/**
	 * 向数据库中添加一个订单项
	 * @param orderItem
	 * @return
	 */
	int  saveOrderItem(OrderItem orderItem);
	
	/**
	 * 根据订单号查找订单项
	 * @param orderId
	 * @return
	 */
	List<OrderItem> getOrderItemByOrderId(String orderId);
	
	/**
	 * 向数据库批量插入订单项的信息
	 * @param params
	 * @return
	 */
	int[] batchSave(Object[][] params);
	


}
