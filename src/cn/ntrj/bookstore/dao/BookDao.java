package cn.ntrj.bookstore.dao;
/**
 * 定义图书的数据库相关操作的接口
 * @author wp
 *
 */

import java.util.List;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Page;

public interface BookDao {
	/**
	 * 向数据库添加一个图书
	 * @param book
	 * @return 返回插入数据的条数
	 */
	int saveBook(Book book);
	
	/**
	 * 根据bookId 删除数据库中的一条图书记录
	 * @param bookId
	 * @return 返回删除数据的条数
	 */
	int delBook(String bookId);
	/**
	 * 更新数据库中的一条图书记录
	 * @param book
	 * @return 返回更新的记录的条数
	 */
	int updateBook(Book book);
	/**
	 * 取出数据库中 图书的所有记录
	 * @return 图书的List集合
	 */
	List<Book> getBookList();
	
	/**
	 * 根据bookId 取出数据库中的一条记录
	 * @param bookId  book的id属性
	 * @return 返回数据库中的一条记录
	 */
	Book getBookById(String bookId);
	
	/**
	 * 分页显示数据库中的数据
	 * @param page
	 * @return
	 */
	Page<Book> findBook(Page<Book> page);
	
	/**'
	 *根据价格查询图书
	 * @param page
	 * @param minPrice
	 * @param maxPrice
	 * @return
	 */
	Page<Book>  findBookByPrice(Page<Book> page,double minPrice,double maxPrice);


	/**
	 * 批量修改图书的销量和库存
	 * @param params
	 * @return int[]
	 */
	int[] batchUpdateSalesAndStock(Object[][] params);
}
