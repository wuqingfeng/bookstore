package cn.ntrj.bookstore.service;

import java.util.List;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Page;
/**
 * 与图书相关的业务
 * @author wp
 *
 */
public interface BookService {
	
	/**
	 * 向数据库添加一个图书
	 * @param book
	 * @return 返回插入数据的条数
	 */
	int saveBook(Book book);
	
	/**
	 * 根据bookId 删除数据库中的一条图书记录
	 * @param bookId
	 * @return 返回删除数据的条数
	 */
	int delBook(String bookId);
	/**
	 * 更新数据库中的一条图书记录
	 * @param book
	 * @return 返回更新的记录的条数
	 */
	int updateBook(Book book);
	/**
	 * 取出数据库中 图书的所有记录
	 * @return 图书的List集合
	 */
	List<Book> getBookList();
	
	/**
	 * 根据bookId 取出数据库中的一条记录
	 * @param bookId  book的id属性
	 * @return 返回数据库中的一条记录
	 */
	Book getBookById(String bookId);
	
	/**
	 * 查找图书分页信息
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	Page<Book> findBook(String pageNumber,int pageSize);
	
	/**
	 * 根据价格查找图书
	 * @param pageNumber
	 * @param pageSize
	 * @param string
	 * @param string2
	 * @return
	 */
	Page<Book>  findBookByPrice(String pageNumber,int pageSize,String min,String max);

}
