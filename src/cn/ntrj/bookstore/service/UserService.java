package cn.ntrj.bookstore.service;

import cn.ntrj.bookstore.bean.User;

/**
 *  定义用于相关业务的接口
 * @author wp
 *
 */

public interface UserService {
	
	/**
	 * 用户登录
	 * @param user
	 * @return 若返回为null  则说明数据库中没有此对象  
	 */
	User login(User user);
	
	
	/**
	 * 用户注册
	 * @param user
	 * @return 若返回值为true  则数据已插入    ；   若为false  则插入失败，用户名已存在
	 *
	 */
	boolean regist(User user);
	
	/**
	 * 检查用户名是否可以  可用 返回true ， 不可用返回false
	 * @param name
	 * @return
	 */
	boolean checkUsername(String username);

}
