package cn.ntrj.bookstore.service.impl;

import cn.ntrj.bookstore.bean.User;
import cn.ntrj.bookstore.dao.UserDao;
import cn.ntrj.bookstore.dao.impl.UserDaoImpl;
import cn.ntrj.bookstore.service.UserService;

/**
 * UserService的实现类
 * @author wp
 *
 */
public class UserServiceImpl implements UserService {

	//创建一个UserDao 对象
	private UserDao userDao=new UserDaoImpl();
	@Override
	public User login(User user) {
		// TODO Auto-generated method stub
		return userDao.getUserByUsernameAndPassword(user);
	}

	/**
	 * 若返回值为true  则数据已插入
	 * 若为false  则插入失败，用户名已存在
	 */
	@Override
	public boolean regist(User user) {
		int count=userDao.SaveUser(user);
		return count>0;
	}

	@Override
	public boolean checkUsername(String username) {
		User user=userDao.getUserByUsername(username);
		return user == null ;
	}

}
