package cn.ntrj.bookstore.service.impl;

import java.util.List;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Page;
import cn.ntrj.bookstore.dao.BaseDao;
import cn.ntrj.bookstore.dao.BookDao;
import cn.ntrj.bookstore.dao.impl.BookDaoImpl;
import cn.ntrj.bookstore.service.BookService;
/**
 * BookService实现类
 * @author wp
 *
 */

public class BookServiceImpl extends BaseDao<Book> implements BookService {

	//创建BookDao 的实例
	private BookDao bookDao=new BookDaoImpl();
	@Override
	public int saveBook(Book book) {
		// TODO Auto-generated method stub
		return bookDao.saveBook(book);
	}

	@Override
	public int delBook(String bookId) {
		// TODO Auto-generated method stub
		return bookDao.delBook(bookId);
	}

	@Override
	public int updateBook(Book book) {
		// TODO Auto-generated method stub
		return bookDao.updateBook(book);
	}

	@Override
	public List<Book> getBookList() {
		// TODO Auto-generated method stub
		return bookDao.getBookList();
	}

	@Override
	public Book getBookById(String bookId) {
		// TODO Auto-generated method stub
		return bookDao.getBookById(bookId);
	}

	/**
	 * findBook创建一个page对象
	 */
	@Override
	public Page<Book> findBook(String pageNumber, int pageSize) {
		int pn=1;
	//	System.out.println("pageNumber"+pageNumber);
		try {
			//一旦出现类型转换异常，则不会赋值     则pn为1
			pn=Integer.parseInt(pageNumber);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		
		//创建一个Page对象
		Page<Book> page=new Page<>();
		page.setPageNumber(pn);
		page.setPageSize(pageSize);
		
		return bookDao.findBook(page);
	}

	

	@Override
	public Page<Book> findBookByPrice(String pageNumber, int pageSize, String min, String max) {
		int pn=1;
		try {
		//将pageNumber转换为int  若转换失败则默认为pn=1  
		 pn=Integer.parseInt(pageNumber);
		}catch (Exception e) {
		}
		
		Page<Book> page=new Page<>();
		page.setPageNumber(pn);
		page.setPageSize(pageSize);
		double minPrice=0;
		try {
			//将pageNumber转换为double  若转换失败则默认为minPrice默认为0
			minPrice=Double.parseDouble(min);
		} catch (Exception e) {
			// TODO: handle exception
		}
		double maxPrice=Double.MAX_VALUE;
		try {
			maxPrice=Double.parseDouble(max);		
		} catch (Exception e) {
			
		}
		

		return bookDao.findBookByPrice(page, minPrice, maxPrice);
	}

	

}
