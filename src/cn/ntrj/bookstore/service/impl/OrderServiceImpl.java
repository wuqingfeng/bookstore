package cn.ntrj.bookstore.service.impl;

import java.util.Date;
import java.util.List;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Cart;
import cn.ntrj.bookstore.bean.CartItem;
import cn.ntrj.bookstore.bean.Order;
import cn.ntrj.bookstore.bean.OrderItem;
import cn.ntrj.bookstore.bean.User;
import cn.ntrj.bookstore.dao.BookDao;
import cn.ntrj.bookstore.dao.OrderDao;
import cn.ntrj.bookstore.dao.OrderItemDao;
import cn.ntrj.bookstore.dao.impl.BookDaoImpl;
import cn.ntrj.bookstore.dao.impl.OrderDaoImpl;
import cn.ntrj.bookstore.dao.impl.OrderItemDaoImpl;
import cn.ntrj.bookstore.service.OrderService;

public class OrderServiceImpl implements OrderService {

	// 分别创建三个DAO
	private OrderDao orderDao = new OrderDaoImpl();
	private OrderItemDao orderItemDao = new OrderItemDaoImpl();
	private BookDao bookDao = new BookDaoImpl();
	

	@Override
	public String createOrder(Cart cart, User user) {
		// 生成一个订单号
		String orderId = System.currentTimeMillis() + "" + user.getId();
		// 读取cart中的信息
		// 获取商品总金额
		double totalAmount = cart.getTotalAmount();
		// 获取商品总数量
		int totalCount = cart.getTotalCount();
		// 创建order 对象
		Order order = new Order(orderId, new Date(), totalCount, totalAmount, 0, user.getId());
		// 将order插进数据库
		orderDao.saveOrder(order);
		// 获取所有的购物项
		List<CartItem> cartItems = cart.getCartItems();
		
		//创建两个二维数组  用来保存book 和orderitem的参数
		Object[][] itemParams=new Object[cartItems.size()][];
		Object[][] bookParams = new Object[cartItems.size()][];
		// 遍历购物项
		for (int i = 0; i < cartItems.size(); i++) {
			// 获取cartItem
			CartItem cartItem = cartItems.get(i);
			// 获取图书信息
			Book book = cartItem.getBook();
			// 获取cartItem中的信息
			// 图书的小计金额
			double amount = cartItem.getAmount();
			// 获取图书数量
			int count = cartItem.getCount();
			// 获取图书信息
			String title = book.getTitle();
			String author = book.getAuthor();
			double price = book.getPrice();
			String imgpath = book.getImg_path();

			// 获取图书的 销量和库存
			int sales = book.getSales();// 销量
			int stock = book.getStock();

			// 创建orderitem
			/*OrderItem orderItem = new OrderItem(null, count, amount, title, author, price, imgpath, orderId);
			// 将orderitem放进数据库
			orderItemDao.saveOrderItem(orderItem);*/
			
			//向数组中添加属性
			itemParams[i]=new Object[] {count, amount, title, author, price, imgpath, orderId};
/*
			book.setSales(sales + count);
			book.setStock(stock - count);

			// 更新数据库
			bookDao.updateBook(book);*/
			//update bs_book set sales = ? ,stock = ? where id = ?
			bookParams[i]=new Object[] {sales + count, stock - count ,book.getId()};
			//判断库存是否充足
			if( stock - count <0 ) {
				//库存不足
				throw new RuntimeException("库存不足");
			}
		}
		
		//批量插入数据到 orderItem
		orderItemDao.batchSave(itemParams);
		
		//批量在book表中  修改 销量和  库存
		bookDao.batchUpdateSalesAndStock(bookParams);
		

		// 清空购物车
		cart.clear();

		// 返回订单号
		return orderId;
	}

	@Override
	public List<Order> getOrderList() {

		return orderDao.getOrderList();
	}

	@Override
	public List<Order> getOrderListByUserId(String userId) {

		return orderDao.getOrderListByUserId(userId);
	}

	@Override
	public void sendBook(String orderId) {
		orderDao.updateOrderState(orderId, 1);

	}

	@Override
	public void takeBook(String orderId) {
		orderDao.updateOrderState(orderId, 2);

	}

	@Override
	public List<OrderItem> getOrderInfo(String orderId) {
		return orderItemDao.getOrderItemByOrderId(orderId);
	}

}
