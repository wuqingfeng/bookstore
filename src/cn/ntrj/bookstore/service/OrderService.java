package cn.ntrj.bookstore.service;

import java.util.List;

import cn.ntrj.bookstore.bean.Cart;
import cn.ntrj.bookstore.bean.Order;
import cn.ntrj.bookstore.bean.OrderItem;
import cn.ntrj.bookstore.bean.User;

/**
 * 处理订单相关业务的接口
 * 
 * @author wp
 *
 */
public interface OrderService {

	/**
	 * 根据购物车信息和用户信息生成一个订单，并返回订单号
	 * 
	 * @param cart
	 * @param user
	 * @return
	 */
	String createOrder(Cart cart, User user);

	/**
	 * 查询所有订单 后台用
	 * 
	 * @return
	 */
	List<Order> getOrderList();

	/**
	 * 根据用户的id信息返回订单信息
	 * 
	 * @param UserId
	 * @return
	 */
	List<Order> getOrderListByUserId(String userId);

	/**
	 * 确认发货 修改订单状态为1 已发货。 管理员调用。
	 * 
	 * @param orderId
	 */
	void sendBook(String orderId);

	/**
	 * 确认收货 修改订单状态为2， 交易完成， 客户调用
	 * 
	 * @param orderId
	 */
	void takeBook(String orderId);

	/**
	 * 查看订单详情，实际上就是查看当前订单下的所有的订单项
	 * 
	 * @param orderId
	 * @return
	 */
	List<OrderItem> getOrderInfo(String orderId);

}
