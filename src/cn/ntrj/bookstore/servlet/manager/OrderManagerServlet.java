package cn.ntrj.bookstore.servlet.manager;

import cn.ntrj.bookstore.bean.Order;
import cn.ntrj.bookstore.service.OrderService;
import cn.ntrj.bookstore.service.impl.OrderServiceImpl;
import cn.ntrj.bookstore.servlet.BaseServlet;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 处理后台订单管理请求的servlet
 * 
 * @author wp
 *
 */
public class OrderManagerServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private OrderService orderService = new OrderServiceImpl();

	/**
	 * 查找所有的订单
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void orderList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 调用OrderService 查询所有订单
		List<Order> list = orderService.getOrderList();
		// 将list 放在域中
		request.setAttribute("list", list);
		// 转发
		request.getRequestDispatcher("/pages/manager/order_manager.jsp").forward(request, response);
	}

	protected void sendBook(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取订单号
		String orderId = request.getParameter("orderId");
		// 调用service 发货
		orderService.sendBook(orderId);
		// 重定向
		response.sendRedirect(request.getContextPath()+
				"/manager/OrderManagerServlet?method=orderList");
	}

}
