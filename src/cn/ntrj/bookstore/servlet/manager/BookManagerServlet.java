package cn.ntrj.bookstore.servlet.manager;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Page;
import cn.ntrj.bookstore.service.BookService;
import cn.ntrj.bookstore.service.impl.BookServiceImpl;
import cn.ntrj.bookstore.servlet.BaseServlet;
import cn.ntrj.bookstore.utils.WEBUtils;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 处理图书相关的请求
 * 
 * @author wp
 *
 */
public class BookManagerServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

	// 创建BookService
	private BookService bookService = new BookServiceImpl();

	/**
	 * 向数据库中添加图书
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void addBook(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 获取要添加的图书 （获取页面传送的请求参数，并封装为book对象）
		Book book = WEBUtils.param2Bean(request, new Book());
	//	System.out.println(book);
		// 调用Service将图书添加到数据库
		bookService.saveBook(book);
		// 将请求重定向到一个页面
		response.sendRedirect(request.getContextPath() + "/manager/BookManagerServlet?method=findBook");
	}

	/**
	 * 添加或者修改图书
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void updateBook(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 获取要添加的图书 （获取页面传送的请求参数，并封装为book对象）
		Book book = WEBUtils.param2Bean(request, new Book());
		if (book.getId() == null || book.getId() == 0) {
			// 如果book对象中没有id属性，则做添加操作
			bookService.saveBook(book);
		} else {
			// 有id属性 做修改操作
			bookService.updateBook(book);
		}
		// System.out.println(book);

		String referer = request.getParameter("referer");

		// 重定向到图书列表页面 能用重定向 绝对不用转发
		response.sendRedirect(referer);
	}

	/**
	 * 获取全部图书
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void bookList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取图书的list
		List<Book> list = bookService.getBookList();
		// 将list放入进request域中
		request.setAttribute("list", list);
		// 转发到列表页面
		request.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(request, response);
	}

	/**
	 * 删除一本图书
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void delBook(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取要删除的 id
		String bookId = request.getParameter("bookId");
		// 调用service 删除图书

		int count = bookService.delBook(bookId);
		System.out.println("删除的条数：" + count);
		/*
		 * 目前是删除结束后默认回到第一页，希望从哪来回哪去 referer请求头 Referer
		 * http://localhost:8080/BookStor…ManagerServlet?method=findBook
		 * 只需要让请求回到referer头所指的地方
		 * 
		 */
		String referer = request.getHeader("referer");
		// System.out.println("referer: "+referer);
		// 重定向到列表页面
		// response.sendRedirect(request.getContextPath()+"/manager/BookManagerServlet?method=findBook");
		response.sendRedirect(referer);
	}

	/**
	 * 去修改图书的页面
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void toUpdatePage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取要修改的图书
		String bookId = request.getParameter("bookId");
		// 调用service查询图书
		Book book = bookService.getBookById(bookId);
		// 将book放入request域中
		request.setAttribute("book", book);
		// 转发到 /pages/manager/book_edit.jsp
		// System.out.println("toUpdatePage:request.setAttribute(\"book\", book);");
		request.getRequestDispatcher("/pages/manager/book_edit.jsp").forward(request, response);
	}

	/**
	 * 根据分页查找图书
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */

	protected void findBook(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取页码
		String pageNumber = request.getParameter("pageNumber");
		// 指定pageSize大小
		int pageSize = 4;
		// 调用service查询分页数据
		Page<Book> page = bookService.findBook(pageNumber, pageSize);

		// 动态获取请求的求地址
		String path = WEBUtils.getPath(request, response);
		// 将路径设置进page对象
		page.setPath(path);
		// 将page放在request域中
		request.setAttribute("page", page);
		// System.out.println("----------------");
		request.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(request, response);
	}

}
