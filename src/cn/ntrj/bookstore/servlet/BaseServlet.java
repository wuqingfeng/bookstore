package cn.ntrj.bookstore.servlet;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * 专门被其它Servlet 继承
 * @author wp
 *
 */

public class BaseServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//处理post请求乱码，只需要在getParamter 方法第一次调用
		request.setCharacterEncoding("utf-8");
		
		//获取用户传递的请求参数
		String methodName=request.getParameter("method");
	//	System.out.println("methodName:"+methodName);
		//通过方法名获取方法的对象
		//获取当前类的Class 对象  this:代表当前类  BaseServlet的子类
	
		Class<?> clas=this.getClass();
		//
		try {
			
			//clas.getDeclaredMethod(name, parameterTypes)  
			//方法名为methodName  参数类型为HttpServletRequest.class,HttpServletResponse.class
			Method method=clas.getDeclaredMethod(methodName, 
					HttpServletRequest.class,HttpServletResponse.class);
			//设置方法的访问权限
			method.setAccessible(true);
			//调用方法
			//invoke 用于调用方法  第一个参数是调用方法的对象， 
			//method.invoke(obj, args) obj:调用方法所在的类对象   args参数
			method.invoke(this, request,response);
		} catch (Exception e) {
			//将异常转换为运行时异常往上抛
			throw new RuntimeException(e);
			//e.printStackTrace();
		} 
				
	}
	
	

}
