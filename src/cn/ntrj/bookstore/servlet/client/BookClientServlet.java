package cn.ntrj.bookstore.servlet.client;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Page;
import cn.ntrj.bookstore.service.BookService;
import cn.ntrj.bookstore.service.impl.BookServiceImpl;
/**
 * 处理前端 图书显示的请求
 * @author wp
 *
 */
import cn.ntrj.bookstore.servlet.BaseServlet;
import cn.ntrj.bookstore.utils.WEBUtils;
public class BookClientServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
    //创建一个BookService对象
	private BookService bookService=new BookServiceImpl();
	
	/**
	 * 前端分页查找图书的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void findBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取页码
		String pageNumber=request.getParameter("pageNumber");
		//设置pagesize
		int pageSize=4;
		//调用service查询page对象
		Page<Book> page=bookService.findBook(pageNumber, pageSize);
		//设置path对象
		page.setPath(WEBUtils.getPath(request, response));
		//把page放进域中
		request.setAttribute("page", page);
		//转发到/pages/book/book-list.jsp
		request.getRequestDispatcher("/pages/book/book-list.jsp").forward(request, response);
	}
	
	/**
	 * 根据价格查找图书
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void findBookByPrice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取页面
		String pageNumber=request.getParameter("pageNumber");
		//设置一个pageSize
		int pageSize=4;
		//获取最大值和最小值
		String min=request.getParameter("min");
		String max=request.getParameter("max");
		//调用Service 查询page对象
		Page<Book> page=bookService.findBookByPrice(pageNumber, pageSize, min, max);
	//	System.out.println(page.getData());
		
		//设置path属性
		page.setPath(WEBUtils.getPath(request, response));
		//将page对象设置进request域中 
		request.setAttribute("page", page);
		//转发到index.jsp
		request.getRequestDispatcher("/pages/book/book-list.jsp").forward(request, response);
	}



}
