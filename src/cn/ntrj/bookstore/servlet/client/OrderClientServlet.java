package cn.ntrj.bookstore.servlet.client;

import cn.ntrj.bookstore.bean.Cart;
import cn.ntrj.bookstore.bean.Order;
import cn.ntrj.bookstore.bean.OrderItem;
import cn.ntrj.bookstore.bean.User;
import cn.ntrj.bookstore.service.OrderService;
import cn.ntrj.bookstore.service.impl.OrderServiceImpl;
import cn.ntrj.bookstore.servlet.BaseServlet;
import cn.ntrj.bookstore.utils.WEBUtils;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 处理用户相关请求的
 * 
 * @author wp
 *
 */
public class OrderClientServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	// 创建一个OrderService
	private OrderService orderService = new OrderServiceImpl();

	/**
	 * 用于结账的方法
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void checkOut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取session对象
		HttpSession session = request.getSession();
		// 获取当前登陆的用户的对象
		User user = (User) session.getAttribute("loginUser");

		// 已经登录 获取购物车对象
		Cart cart = WEBUtils.getCart(request);
		// 调用service生成一个订单
		String orderId = orderService.createOrder(cart, user);

		// 将订单号放在域对象中
		request.setAttribute("orderId", orderId);
		// 转发
		request.getRequestDispatcher("/pages/cart/checkout.jsp").forward(request, response);

	}

	/**
	 * 查询当前用户的所有订单
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void orderList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取session对象
		HttpSession session = request.getSession();
		// 获取user对象
		User user = (User) session.getAttribute("loginUser");

		List<Order> list = orderService.getOrderListByUserId(user.getId() + "");

		request.setAttribute("list", list);

		// 转发
		request.getRequestDispatcher("/pages/order/order.jsp").forward(request, response);

	}

	protected void takeBook(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取订单号
		String orderId = request.getParameter("orderId");
		// 调用service进行收货
		orderService.takeBook(orderId);
		// 重定向
		response.sendRedirect(request.getContextPath() + "/client/OrderClientServlet?method=orderList");

	}

	/**
	 * 查看当前订单下的所有的订单项 查看订单详情
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void orderInfo(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 获取订单号
		String orderId = request.getParameter("orderId");
		// 调用service获取订单详情
		List<OrderItem> list = orderService.getOrderInfo(orderId);
		// 将订单详情放在request域中
		request.setAttribute("list", list);
		// 转发
		request.getRequestDispatcher("/pages/order/order-info.jsp").forward(request, response);
	}

}
