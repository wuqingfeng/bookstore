package cn.ntrj.bookstore.servlet.client;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

import cn.ntrj.bookstore.bean.User;
import cn.ntrj.bookstore.service.UserService;
import cn.ntrj.bookstore.service.impl.UserServiceImpl;
import cn.ntrj.bookstore.servlet.BaseServlet;
import cn.ntrj.bookstore.utils.WEBUtils;

/**
 * 处理与用户有关的Servlet   
 * 继承于BaseServlet
 * @author wp
 *
 */
public class UserServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private UserService userService=new UserServiceImpl();
   
	public void regist(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		//获取session
		HttpSession session= request.getSession();
		//获取用户填写的验证码
		String regCode=request.getParameter("code1");
		//获取session中的验证码
		String sessCode=(String) session.getAttribute("KAPTCHA_SESSION_KEY");
	//	System.out.println(sessCode);
		//移除session中的验证码
		session.removeAttribute(sessCode);
		//检查验证码是否正确
		if (regCode !=null && regCode.toUpperCase().equals(sessCode.toUpperCase()) ) {
			//获取用户输入的信息
			User user=new User();
			//获取输入框中输入的值
			user=WEBUtils.param2Bean(request, user);
			if(userService.regist(user)) {
				//重定向到成功页面
				response.sendRedirect(request.getContextPath()+"/pages/user/regist_success.jsp");
			}else {
				//转发到原页面并提示注册失败  用户名已存在
				request.setAttribute("msg", "注册失败，用户名已存在");
				request.setAttribute("username", user.getUsername());
				request.setAttribute("password", user.getPassword());
				request.setAttribute("email", user.getEmail());
				request.getRequestDispatcher("/pages/user/regist.jsp").forward(request, response);
				
			}
		}else {
			
			//转发到原页面并提示注册失败  验证码错误
			request.setAttribute("msg", "验证码错误");
			request.getRequestDispatcher("/pages/user/regist.jsp").forward(request, response);
		}
		
		
		
		
		
	}

	public void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取session对象
		HttpSession session=request.getSession();
		//调用Service 验证用户名和密码
		User user=new User();
		user=WEBUtils.param2Bean(request, user);
		User loginUser=userService.login(user);
		//如果loginUser 为null  用户名或密码错误，转发到login.html
		if(loginUser ==null) {
			//登录失败 设置一个错误消息  用户名和密码错误
			request.setAttribute("msg", "用户名和密码错误");
			request.getRequestDispatcher("/pages/user/login.jsp").forward(request, response);
		}else {
			//登录成功  将loginUser放大session域中
			session.setAttribute("loginUser", loginUser);
			//用户名和密码正确，重定向到login-success.html
			response.sendRedirect(request.getContextPath()+
							"/pages/user/login_success.jsp");
		}
		
	}
	
	public void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取session对象
		HttpSession session=request.getSession();
		//强制session失效
		session.invalidate();
		//重定向到首页
		response.sendRedirect(request.getContextPath()+"/index.jsp");
	
	}
	
	/**
	 * 检查用户名是否可用
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void checkUsername(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//获取用户填写的用户名
		String username= request.getParameter("username");
	//	System.out.println(username);
		//调用service检查用户名
		boolean flag= userService.checkUsername(username);
		if(flag) {
			//用户名可用
			response.getWriter().print(1);
		}else {
			//用户名不可用
			response.getWriter().print(0);
		}
	}

}
