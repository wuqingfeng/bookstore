package cn.ntrj.bookstore.servlet.client;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Cart;
import cn.ntrj.bookstore.bean.CartItem;
import cn.ntrj.bookstore.service.BookService;
import cn.ntrj.bookstore.service.impl.BookServiceImpl;
import cn.ntrj.bookstore.servlet.BaseServlet;
import cn.ntrj.bookstore.utils.WEBUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

/**
 * 处理购物车相关请求的Servlet
 * @author wp
 *
 */
public class CartServlet extends BaseServlet {
	
	private static final long serialVersionUID = 1L;
	//创建一个BookService 的对象
	private BookService bookService=new BookServiceImpl();

	/**
	 * 向购物车添加一本书
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void addToCart(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//处理响应的乱码问题
		response.setContentType("text/html;charset=urt-8");
		
		//从session中获取购物车对象 
		Cart cart=WEBUtils.getCart(request);
		// 获取图书的id
		String bookId = request.getParameter("bookId");
		
		// 查询图书对象
		Book book = bookService.getBookById(bookId);
		//System.out.println(book);
		// 向购物车中添加一本书
		cart.addBookToCart(book);
		
		int totalCount=cart.getTotalCount();
		//System.out.println(totalCount);
		//获取刚刚添加进购物车的图书的名字
		String title=book.getTitle();
		//将totalcount和title放在一个map中 
		Map<String, Object> map=new HashMap<>();
		map.put("totalCount", totalCount);
		map.put("title", title);

	//	System.out.println(map);
		//将map转换为json字符串
		String json=new Gson().toJson(map);
		//System.out.println(json);
		//json发送到页面  将json 作为响应发送
		response.setCharacterEncoding("UTF-8");
	    //设置页面以什么编码格式显示，这里设置为UTF-8
	    response.setContentType("text/html;charset=UTF-8");
	    // 是每个请求进来的话就发送给每个请求的客户端
		response.getWriter().print(json);	
		
			
		/*// 将书名放进域对象中
		request.getSession().setAttribute("title", book.getTitle());

		// 获取referer 请求头
		String referer = request.getHeader("referer");
		// 重定向到首页

		response.sendRedirect(referer);
*/
	}

	/**
	 * 清空购物车
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void clear(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//从session对象中获取cart对象
		Cart cart=WEBUtils.getCart(request);
		//清空cart 
		cart.clear();
		
		//重定向到首页（暂定）
		response.sendRedirect(request.getContextPath()+"/pages/cart/cart.jsp");
	}
	
	/**
	 * 删除指定的购物项
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void delCartItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//获取要删除的bookId
		String bookId=request.getParameter("bookId");
		
		//获取session对象中的cart对象
		Cart cart=WEBUtils.getCart(request);
		//删除指定bookId的书籍
		cart.delCartItem(bookId);

		response.sendRedirect(request.getContextPath()+"/pages/cart/cart.jsp");
		
	}
	
	/**
	 * 修改购物车中指定的商品的数量
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void updateCount(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//获取要修改的图书的id
		String  bookId=request.getParameter("bookId");
		//获取要修改成的数量
		String countStr=request.getParameter("count");
		//获取购物车对象
		Cart cart=WEBUtils.getCart(request);
		//修改数量
		cart.updateCount(bookId, countStr);
		
		//重定向到/pages/cart/cart.jsp
	//	response.sendRedirect(request.getContextPath()+"/pages/cart/cart.jsp");
		
		int totalCount =cart.getTotalCount();
		
		double totalAmount = cart.getTotalAmount();
		//获取所以的cartItem 的map
		Map<String, CartItem> map=cart.getMap();
		
	
		//根据id获取cartItem 的对象
		CartItem cartItem=map.get(bookId);
		//获取amount
		double amount= cartItem.getAmount();
		
		//创建一个map 对象
		Map<String,Object> jsonMap = new HashMap<>();
		//向jsonMap 中添加数据
		jsonMap.put("totalCount", totalCount + "");
		jsonMap.put("totalAmount", totalAmount + "");
		jsonMap.put("amount", amount + "");
		String json = new Gson().toJson(jsonMap);
		
		response.setCharacterEncoding("UTF-8");
	    //设置页面以什么编码格式显示，这里设置为UTF-8
	    response.setContentType("text/html;charset=UTF-8");
		//将字符串转换作为响应发送
		response.getWriter().print(json);
		
	}

}
