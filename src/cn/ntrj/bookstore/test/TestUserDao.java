package cn.ntrj.bookstore.test;

import org.junit.Test;

import cn.ntrj.bookstore.bean.User;
import cn.ntrj.bookstore.dao.UserDao;
import cn.ntrj.bookstore.dao.impl.UserDaoImpl;

public class TestUserDao {
	
	UserDao ud=new UserDaoImpl();
	
	@Test
	public void testSaveUser() {
		User user=new User(null, "username", "112", "admin@ntu.edu");
		int count=ud.SaveUser(user);
		System.out.println(count);
	}
	
	@Test
	public void testGetUserByUsernameAndPassword() {
		User user=new User(null,"2", "2", "2");
		System.out.println(ud.getUserByUsernameAndPassword(user));
	}
	

}
