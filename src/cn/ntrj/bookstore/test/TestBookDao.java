package cn.ntrj.bookstore.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Page;
import cn.ntrj.bookstore.dao.BookDao;
import cn.ntrj.bookstore.dao.impl.BookDaoImpl;

class TestBookDao {
	BookDao bookDao=new BookDaoImpl();
	
	public void testFindBookByPrice() {
		Page<Book> page=new Page<>();
		page.setPageNumber(1);
		page.setPageSize(4);
		Page<Book> pg=bookDao.findBook(page);
		List<Book> list=pg.getData();
		for (Book book : list) {
			System.out.println(book);
		}
	}
	
	
	@Test
	public void testFindBook() {
		Page<Book> page=new Page<>();
		page.setPageNumber(1);
		page.setPageSize(4);
		Page<Book> pg=bookDao.findBook(page);
		List<Book> list=pg.getData();
		for (Book book : list) {
			System.out.println(book);
		}
		
	
	}
	
	
	@Test
	public void testBatch() {
		//创建一个二维数组
		Object[][] params=new Object[2][];
		params[0]=new Object[] {300,400,41};
		params[1]=new Object[] {200,600,43};
		int [] counts=bookDao.batchUpdateSalesAndStock(params);
		for (int i = 0; i < counts.length; i++) {
			System.out.print(counts[i] + "   ");
		}
	}
	
	@Test
	void testSaveBook() {
		Book book=new Book(null, "西游记", "吴承恩", 100.00, 800, 1000, null);
		int count=bookDao.saveBook(book);
		System.out.println(count);
	}

	@Test
	void testDelBook() {
		int count=bookDao.delBook("2");
		System.out.println(count);
	}

	@Test
	void testUpdateBook() {
		Book book=new Book(2, "西游记", "吴承恩", 80, 8000000, 100000000, null);
		int count=bookDao.updateBook(book);
		System.out.println(count);
	}

	@Test
	void testGetBookList() {
		List<Book> list=bookDao.getBookList();
		System.out.println(list);
	}

	@Test
	void testGetBookById() {
		Book book=bookDao.getBookById("3");
		System.out.println(book);
		
	}

}
