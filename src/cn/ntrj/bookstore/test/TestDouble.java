package cn.ntrj.bookstore.test;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

class TestDouble {

	@Test
	void test() {
		int a=1;
		for (int i = 1; i < 10; i++) {
			
			a*=i;
		}
		System.out.println(a);
	}
	
	@Test
	public void testDouble() {
		double a=0.09;
		double b=0.01;
		System.out.println(a+b);
	}
	
	@Test
	void testInt() {
		//BigDecimal  专门用来做数字的计算
		BigDecimal a=new BigDecimal(1);
		for (int i = 1; i < 50; i++) {
			BigDecimal b=new BigDecimal(i);
			a=a.multiply(b);
		}
	
		System.out.println(a);
	}
	
	@Test
	public void testDouble2() {
		BigDecimal a=new BigDecimal("0.09");
		BigDecimal b=new BigDecimal("0.01");
		System.out.println(a.add(b));
	}

}
