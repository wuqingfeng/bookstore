package cn.ntrj.bookstore.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Page;

class TestPage {

	@Test
	void test() {
		Page<Book> page=new Page<>();
		page.setPageNumber(1);
		page.setPageSize(4);
		page.setTotalRecord(35);
		System.out.println("当前索引:"+page.getIndex());
		System.out.println("总页数："+page.getTotalPage());
		
	}
	
	@Test
	void testMath() {
		double t=Math.ceil(35d/4d);
		System.out.println(35/4);
		System.out.println(t);
		System.out.println((int)t);
		 
		System.out.println((int)Math.ceil(4.0));
		System.out.println((int)Math.ceil(3.1));
		System.out.println((int)Math.ceil(3.5));
		System.out.println((int)Math.ceil(3.9));
	}

}
