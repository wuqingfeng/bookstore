package cn.ntrj.bookstore.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import cn.ntrj.bookstore.bean.OrderItem;
import cn.ntrj.bookstore.dao.OrderItemDao;
import cn.ntrj.bookstore.dao.impl.OrderItemDaoImpl;

class TestOrderItemDao {
	OrderItemDao orderItemDao=new OrderItemDaoImpl();
	@Test
	void testSaveOrderItem() {
		OrderItem orderItem=new OrderItem(null, 20, 20.5, "平凡的世界", "路遥", 20.1, "img_path", "15324027471951");
		
		int count=orderItemDao.saveOrderItem(orderItem);
		System.out.println(count);
	}

	@Test
	void testGetOrderItemByOrderId() {
		List<OrderItem> list = orderItemDao.getOrderItemByOrderId("15324027471951");
		System.out.println(list);
	}

}
