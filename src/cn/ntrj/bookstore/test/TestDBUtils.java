package cn.ntrj.bookstore.test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;

import org.junit.jupiter.api.Test;

import cn.ntrj.bookstore.utils.JDBCUtils;

class TestDBUtils {

	@Test
	public void testDBUtils() {
		Connection conn=JDBCUtils.getConnection();
		System.out.println(conn);
		
		JDBCUtils.releaseConnection(conn);
	}

}
