package cn.ntrj.bookstore.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Page;
import cn.ntrj.bookstore.service.BookService;
import cn.ntrj.bookstore.service.impl.BookServiceImpl;

class TestBookServlet {
	BookService bookService=new BookServiceImpl();
	@Test
	void test() {
		Page<Book> page=bookService.findBookByPrice("1", 4, "10", "20");
		List<Book> book=page.getData();
		for (Book b : book) {
			System.out.println(b);
		}
	}

}
