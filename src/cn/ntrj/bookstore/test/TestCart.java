package cn.ntrj.bookstore.test;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Cart;
import cn.ntrj.bookstore.bean.CartItem;

class TestCart {

	@Test
	void test() {
		Cart cart=new Cart();
		Book book=new Book(11, "fads", "fdsa", 1.01, 10, 10, "");
		Book book2=new Book(233, "fads", "fdsa",10.02, 10, 10, "");
		Book book3=new Book(3, "fads", "fdsa", 20.935, 10, 10, "");
		cart.addBookToCart(book);
		cart.addBookToCart(book2);
		cart.addBookToCart(book3);
		System.out.println("总数量："+cart.getTotalCount());
		System.out.println("总金额："+cart.getTotalAmount());
		
		System.out.println("---------map--------------");
		Map<String, CartItem> map=cart.getMap();
		for (Map.Entry<String, CartItem> c : map.entrySet()) {
			System.out.println(c.getKey()+"  "+c.getValue());
		}
		
		cart.delCartItem("1");
		System.out.println("删除1号");
		System.out.println("总数量："+cart.getTotalCount());
		System.out.println("总金额："+cart.getTotalAmount());
		System.out.println("---------list--------------");
		List<CartItem> cartItems=cart.getCartItems();
		for (CartItem cartItem : cartItems) {
			System.out.println(cartItem.toString());
		}
		
		cart.clear();
		System.out.println("清空购物车");
		
		for (Map.Entry<String, CartItem> c : map.entrySet()) {
			System.out.println(c.getKey()+"  "+c.getValue());
		}
	}

}
