package cn.ntrj.bookstore.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.sun.org.apache.xpath.internal.operations.Or;

import cn.ntrj.bookstore.bean.Order;
import cn.ntrj.bookstore.dao.OrderDao;
import cn.ntrj.bookstore.dao.impl.OrderDaoImpl;

class TestOrderDao {
	OrderDao orderDao=new OrderDaoImpl();
	
	@Test
	void testSaveOrder() {
		String id=System.currentTimeMillis() +""+1;
		Order order=new  Order(id, new Date(), 11, 100, 0, 2);
		int count=orderDao.saveOrder(order);
		System.out.println(count);
	}

	@Test
	void testUpdateOrderState() {
		int count=orderDao.updateOrderState("15324027471951", 2);
		System.out.println(count);
	}

	@Test
	void testGetOrderList() {
		List<Order> list=orderDao.getOrderList();
		System.out.println(list);
	}

	@Test
	void testGetOrderListByUserId() {
		List<Order> list=orderDao.getOrderListByUserId("2");
		System.out.println(list);
	}

}
