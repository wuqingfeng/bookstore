package cn.ntrj.bookstore.test;

import org.junit.jupiter.api.Test;

import cn.ntrj.bookstore.bean.Book;
import cn.ntrj.bookstore.bean.Cart;
import cn.ntrj.bookstore.bean.User;
import cn.ntrj.bookstore.dao.BookDao;
import cn.ntrj.bookstore.dao.impl.BookDaoImpl;
import cn.ntrj.bookstore.service.OrderService;
import cn.ntrj.bookstore.service.impl.OrderServiceImpl;

class TestOrdrService {
	BookDao bookDao=new BookDaoImpl();
	OrderService orderService=new OrderServiceImpl();

	@Test
	void testCreateOrder() {
		// 创建一个cart
		Cart cart = new Cart();
		// 从数据库中查询图书
		Book book1 = bookDao.getBookById("41");
		Book book2 = bookDao.getBookById("43");
		Book book3 = bookDao.getBookById("44");
		// 购买图书
		cart.addBookToCart(book1);
		cart.addBookToCart(book2);
		cart.addBookToCart(book3);
		// 创建user
		User user = new User(3, null, null, null);
		// 生成一个订单
		String orderId = orderService.createOrder(cart, user);
		System.out.println(orderId);
	}

}
