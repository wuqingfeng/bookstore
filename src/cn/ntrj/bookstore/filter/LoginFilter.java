package cn.ntrj.bookstore.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.ntrj.bookstore.bean.Cart;
import cn.ntrj.bookstore.bean.User;
import cn.ntrj.bookstore.utils.WEBUtils;

/**
 * 对 向发送到OrderClientServlet 的请求进行统一的登录验证
 * 
 * @author wp
 *
 */
public class LoginFilter extends HttpFilter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// 获取session对象
		HttpSession session = request.getSession();
		// 获取当前登陆的用户的对象
		User user = (User) session.getAttribute("loginUser");
		// 判断User 是否为null
		if (user == null) {
			// 设置一个错误消息
			request.setAttribute("msg", "该操作需要用户登录");
			request.getRequestDispatcher("/pages/user/login.jsp").forward(request, response);
		} else {
			// 放行
			chain.doFilter(request, response);

		}

	}

}
