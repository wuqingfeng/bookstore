package cn.ntrj.bookstore.filter;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.ntrj.bookstore.utils.JDBCUtils;

/**
 * 用于统一管理实务的filter
 * 
 * @author wp
 *
 */
public class TransactionFilter extends HttpFilter implements Filter {

	public TransactionFilter() {
		// TODO Auto-generated constructor stub
	}

	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// 获取数据库连接
		Connection conn = JDBCUtils.getConnection();
		try {
			// 设置手动提交事务
			conn.setAutoCommit(false);
			// 放行请求
			chain.doFilter(request, response);
			// 正常处理
			conn.commit();
		} catch (Exception e) {
			e.printStackTrace();
			// 如果有异常 则回滚事务
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			//设置一个错误消息
			request.setAttribute("error", "系统出现异常，请联系管理员");
			//转发到错误页面
			request.getRequestDispatcher("/pages/error/error.jsp").forward(request, response);
		} finally {
			// 关闭数据库连接
			 JDBCUtils.releaseConnection();
		}

	}

}
